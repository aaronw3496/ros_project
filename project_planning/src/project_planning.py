#! /usr/bin/env python
import rospy
import time
import actionlib
from move_base_msgs.msg import MoveBaseAction, MoveBaseGoal, MoveBaseResult, MoveBaseFeedback


# definition of the feedback callback. This will be called when feedback
# is received from the action server
# it just prints a message indicating a new message has been received
def feedback_callback(feedback):
    print('[Feedback] Going to Goal Pose...')

# initializes the action client node
rospy.init_node('move_base_action_client')
# create the connection to the action server
client = actionlib.SimpleActionClient('/move_base', MoveBaseAction)
# waits until the action server is up and running
client.wait_for_server()
# creates a goals to send to the action server
goal = MoveBaseGoal()
goal.target_pose.header.frame_id = 'map'
goal.target_pose.header.stamp = rospy.Time.now()
goal.target_pose.pose.position.x = 2.0
goal.target_pose.pose.position.y = 1.0
goal.target_pose.pose.position.z = 0.0
goal.target_pose.pose.orientation.x = 0.0
goal.target_pose.pose.orientation.y = 0.0
goal.target_pose.pose.orientation.z = 0.0
goal.target_pose.pose.orientation.w = 1.0

goal2 = MoveBaseGoal()
goal2.target_pose.header.frame_id = 'map'
goal.target_pose.header.stamp = rospy.Time.now()
goal2.target_pose.pose.position.x = 5.03
goal2.target_pose.pose.position.y = 1.03
goal2.target_pose.pose.position.z = 0.0
goal2.target_pose.pose.orientation.x = 0.0
goal2.target_pose.pose.orientation.y = 0.0
goal2.target_pose.pose.orientation.z = -0.70
goal2.target_pose.pose.orientation.w = 0.71

goal3 = MoveBaseGoal()
goal3.target_pose.header.frame_id = 'map'
goal.target_pose.header.stamp = rospy.Time.now()
goal3.target_pose.pose.position.x = 4.98
goal3.target_pose.pose.position.y = -2.96
goal3.target_pose.pose.position.z = 0.0
goal3.target_pose.pose.orientation.x = 0.0
goal3.target_pose.pose.orientation.y = 0.0
goal3.target_pose.pose.orientation.z = 0.99
goal3.target_pose.pose.orientation.w = -0.037

goal4 = MoveBaseGoal()
goal4.target_pose.header.frame_id = 'map'
goal.target_pose.header.stamp = rospy.Time.now()
goal4.target_pose.pose.position.x = 1.99
goal4.target_pose.pose.position.y = -3.0
goal4.target_pose.pose.position.z = 0.0
goal4.target_pose.pose.orientation.x = 0.0
goal4.target_pose.pose.orientation.y = 0.0
goal4.target_pose.pose.orientation.z = 0.70
goal4.target_pose.pose.orientation.w = 0.71

# sends the goals to the action server, specifying which feedback function
# to call when feedback received
client.send_goal(goal, feedback_cb=feedback_callback)
#Waiting for the client to finish and precise a timeout parameter
client.wait_for_result(rospy.Duration(20.0))
print('[Result] State: %d' % (client.get_state()))

client.send_goal(goal2, feedback_cb=feedback_callback)
client.wait_for_result(rospy.Duration(25.0))
print('[Result] State: %d' % (client.get_state()))

client.send_goal(goal3, feedback_cb=feedback_callback)
client.wait_for_result(rospy.Duration(30.0))
print('[Result] State: %d' % (client.get_state()))

client.send_goal(goal4, feedback_cb=feedback_callback)
client.wait_for_result(rospy.Duration(30.0))
print('[Result] State: %d' % (client.get_state()))
